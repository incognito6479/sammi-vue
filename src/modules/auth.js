const state = {
    isLoading: false 
}
const mutations = {
    setLoader(state) {
        state.isLoading = true 
    } 
}

export default {
    state,
    mutations
}