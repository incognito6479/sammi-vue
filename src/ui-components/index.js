import Button from '@/ui-components/Button.vue'
import Input from '@/ui-components/Input.vue'
import Logo from '@/ui-components/Logo.vue'

export default [Button, Input, Logo]